# aio-bundle

Set with all tools, that you need.

##### versions

- [ansible](https://www.ansible.com/) 2.8.1
- [mitogen](https://networkgenomics.com/ansible/) 0.2.6
- [requests](https://pypi.org/project/requests/) 2.22.0
- [terraform](https://www.terraform.io/) 0.11.13
- [terraform-provider-libvirt](https://github.com/dmacvicar/terraform-provider-libvirt) 99d65d7b948eb221b40116e203efa705eed5ca50
- [terraform-provisioner-ansible](https://github.com/radekg/terraform-provisioner-ansible) v2.1.2
- [terraform-inventory](https://github.com/adammck/terraform-inventory) v0.8
- kubectl v1.14.1
- helm v2.12.0
- [kubeval](https://github.com/garethr/kubeval) 0.7.3
- [gitlab-wait-for](https://gitlab.com/egeneralov/gitlab-wait-for) 1.0.0

##### packages

- openssh-client
- xorriso
- git
- jq
- curl
- qemu-img
- unzip
- make

##### ansible roles

- [egeneralov.docker](https://github.com/egeneralov/docker) snapshot 06/20/19
- [egeneralov.lxc](https://github.com/egeneralov/lxc) snapshot 06/20/19
- [egeneralov.gitlab](https://github.com/egeneralov/gitlab) snapshot 06/20/19
- [egeneralov.sshd_config](https://github.com/egeneralov/sshd_config) snapshot 06/20/19
- [egeneralov.gitlab-runner](https://github.com/egeneralov/gitlab-runner) snapshot 06/20/19
- [egeneralov.peervpn](https://github.com/egeneralov/peervpn) snapshot 06/20/19
- [egeneralov.python](https://github.com/egeneralov/python) snapshot 06/20/19
- [egeneralov.i2pd](https://github.com/egeneralov/i2pd) snapshot 06/20/19
- [egeneralov.postgresql-repository](https://github.com/egeneralov/postgresql-repository) snapshot 06/20/19
- [egeneralov.postgresql](https://github.com/egeneralov/postgresql) snapshot 06/20/19
- [egeneralov.timescaledb](https://github.com/egeneralov/timescaledb) snapshot 06/20/19
